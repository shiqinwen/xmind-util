# xmind-util

#### 介绍
xmind解析为json和根据json导出xmind文件

#### 软件架构
软件架构说明：
xmind解析为json，对象，
    - 支持新版xmindzen，旧版xmind8
    - 支持旧版中xml的comments批注转换
    - 代码简单易使用
    - 剔除多余节点，简化Notes节点，title节点

#### 使用说明

1.  本代码主要是解析和导出xmind，可以导出指定的主题关联的节点
2.  没有带样式导出，样式需要解析style.xml文件，本demo没有做
3.  解析是Example这个类入口，demo中一个demo.xmind进行解析
    解析后数据是类似于一下的json数据
    ![输入图片说明](image.png) 

4.导出是CreateXmind入口，以XmindRoot对象的json格式数据进行导出
    会导出带有 **笔记(备注)、图标、标签、超链接(网络、文件、主题)** ，其他如批注 需要解析comments.xml，还有的可能也不支持
    ![输入图片说明](doc/%E4%B8%BB%E9%A2%98%E6%A0%B8%E5%BF%83.png)
5.关于导出的额外知识点
    Xmind 提供创建、操作、保存思维导图的功能，这些功能的接口都在核心组件 org.xmind.core中定义，这里放几个核心的接口：
    
1. - org.xmind.core.IWorkbook - 代表一个完整的思维导图，其中可能包括多个工作表和样式信息，以及图像、附件和其他文件。
 -     org.xmind.core.ISheet – 代表思维导图中的单个工作表。每张纸都有一个中心主题。
 -     org.xmind.core.ITopic – 代表思维导图上工作表中的一个主题。一个主题只有一个父主题（除非它是工作表的中心主题），并且许多主题有任意数量的子主题。除了这些主界面之外，还有几个用于特定目的。其中一些是：
 -     org.xmind.core.IRelationship – 代表任意两个主题之间的关系
 -     org.xmind.core.IControlPoint – 表示用于设置曲线的关系上的一个点
 -     org.xmind.core.IBoundary – 一组相邻主题
 -     org.xmind.core.ISummary – 类似于边界，但附加了一个主题节点
 -     org.xmind.core.INumbering – 附加到主题及其子项的编号模式
 -     org.xmind.core.ITopicExtension – 允许将自定义扩展添加到主题
 -     org.xmind.core.IManifest – 控制用于添加附件的 xmind 文档清单
 -     org.xmind.core.marker.IMarker - 小图标添加到主题
 -     org.xmind.core.marker.IMarkerRef - 参考标记
 -     org.xmind.core.marker.IMarkerResource - 用于标记图标图像的资源


    最后，还有一些需要静态方法和字段的基类：
        org.xmind.core.Core – 通用事件定义，以及用于实例化许多接口的静态方法
        org.xmind.core.util.Point – 用于设置分离主题的坐标
        org.xmind.ui.style.Styles – 包含属性和值的所有样式定义
下表列出了各种可用的结构，以及每个结构的类。


- 结构类型	结构类名
- 地图（默认）	“”-空字符串
- 左头鱼骨	org.xmind.ui.fishbone.leftHeaded
- 右头鱼骨	org.xmind.ui.fishbone.rightHeaded
- 左逻辑图	org.xmind.ui.logic.left
- 正确的逻辑图	org.xmind.ui.logic.right
- 向下组织图	org.xmind.ui.org-chart.down
- 向上组织图	org.xmind.ui.org-chart.up
- 电子表格	org.xmind.ui.spreadsheet
- 左树	org.xmind.ui.tree.left
- 右树	org.xmind.ui.tree.right
- 浮动	org.xmind.ui.map.floating

可参考：http://blog.itpub.net/70001864/viewspace-2790266/
