package com.lst.xmind;

import com.lst.xmind.parser.XmindParseUtils;

/**
 *
 * @Classname Example
 * @Description 测试例子
 */
public class Example {

    public static void main(String[] args) throws Exception {
        String[] files = new String[]{"D:\\workCatalog\\idea_project2\\xmind-util\\doc\\demo.xmind"};
        for (String file:files) {
            String res = XmindParseUtils.parseText(file);
            System.out.println(res);
            System.out.println();
        }
    }
}
