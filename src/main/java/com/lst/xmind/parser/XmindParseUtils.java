package com.lst.xmind.parser;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import com.alibaba.fastjson.JSON;

import com.lst.xmind.entity.XmindRoot;
import org.dom4j.*;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;

/**
 *解析xmind工具类
 */
public class XmindParseUtils {
    public static final String xmindZenJson = "content.json";
    public static final String xmindLegacyContent = "content.xml";
    public static final String xmindLegacyComments = "comments.xml";

    public static String parseText(String xmindFile) throws Exception {
        String res = ZipUtils.extract(xmindFile);
        String content = null;
        //兼容新旧版本
        if (isXmindZen(res, xmindFile)) {
            // xmind zen版本   或 xmind11版本
            content = getXmindZenContent(xmindFile,res);
        } else {
            //TODO  一般是xmind8
            content = getXmindLegacyContent(xmindFile,res);
        }
        File dir = new File(res);
        deleteDir(dir);
        XmindRoot XmindRoot = JSON.parseObject(content, XmindRoot.class);
       return(JSON.toJSONString(XmindRoot,false));
    }

    public static Object parseObject(String xmindFile) throws Exception {
        String content = parseText(xmindFile);
        XmindRoot XmindRoot = JSON.parseObject(content, XmindRoot.class);
        return XmindRoot;
    }


    public static boolean deleteDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            //递归删除目录中的子目录下
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        // 目录此时为空，可以删除
        return dir.delete();
    }


    /**
     * @return
     */
    public static String getXmindZenContent(String xmindFile,String extractFileDir) throws Exception {
        List<String> keys = new ArrayList<>();
        keys.add(xmindZenJson);
        Map<String, String> map = ZipUtils.getContents(keys, xmindFile,extractFileDir);
        String content = map.get(xmindZenJson);
        content = XmindZenUtils.getContent(content);
        return content;
    }

    /**
     * @return
     */
    public static String getXmindLegacyContent(String xmindFile,String extractFileDir) throws Exception {
        List<String> keys = new ArrayList<>();
        keys.add(xmindLegacyContent);
        keys.add(xmindLegacyComments);
        Map<String, String> map = ZipUtils.getContents(keys, xmindFile,extractFileDir);

        String contentXml = map.get(xmindLegacyContent);
        String commentsXml = map.get(xmindLegacyComments);
        String xmlContent = getContent(contentXml, commentsXml);

        return xmlContent;
    }

    private static boolean isXmindZen(String res, String xmindFile){
        //解压
        File parent = new File(res);
        if (parent.isDirectory()) {
            String[] files = parent.list(new ZipUtils.FileFilter());
            for (int i = 0; i < Objects.requireNonNull(files).length; i++) {
                if (files[i].equals(xmindZenJson)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 将xml转换成json
     * @param xmlContent
     * @param xmlComments
     * @return
     * @throws Exception
     */
    public static String getContent(String xmlContent, String xmlComments) throws Exception {
        //删除content.xml里面不能识别的字符串
        xmlContent = xmlContent.replace("xmlns=\"urn:xmind:xmap:xmlns:content:2.0\"", "");
        xmlContent = xmlContent.replace("xmlns:fo=\"http://www.w3.org/1999/XSL/Format\"", "");
        //删除<topic>节点
        xmlContent = xmlContent.replace("<topics type=\"attached\">", "");
        xmlContent = xmlContent.replace("</topics>", "");

        if(xmlComments.contains("<topics type=\"callout\">")){
            // 不支持解析 标注
            throw new RuntimeException("不支持解析标注,请修改xmind文件");
        }

        //去除title中svg:width属性
        xmlContent = xmlContent.replaceAll("<title svg:width=\"[0-9]*\">", "<title>");

        Document document = DocumentHelper.parseText(xmlContent);// 读取XML文件,获得document对象
        Element root = document.getRootElement();
        List<Node> topics = root.selectNodes("//topic");

        if (xmlComments != null) {
            //删除comments.xml里面不能识别的字符串
            xmlComments = xmlComments.replace("xmlns=\"urn:xmind:xmap:xmlns:comments:2.0\"", "");
            Document commentDocument = DocumentHelper.parseText(xmlComments);
            List<Node> commentsList = commentDocument.selectNodes("//comment");
            for (Node topic : topics) {
                for (Node commentNode : commentsList) {
                    Element commentElement = (Element) commentNode;
                    Element topicElement = (Element) topic;
                    if (topicElement.attribute("id").getValue().equals(commentElement.attribute("object-id").getValue())) {
                        Element comment = topicElement.addElement("comments");
                        comment.addAttribute("creationTime", commentElement.attribute("time").getValue());
                        comment.addAttribute("author", commentElement.attribute("author").getValue());
                        comment.addAttribute("content", commentElement.element("content").getText());
                    }
                }

            }
        }
        Node rootTopic = root.selectSingleNode("/xmap-content/sheet/topic");
        rootTopic.setName("rootTopic");
        List<Node> topicList = rootTopic.selectNodes("//topic");
        for (Node node : topicList) {
            node.setName("attached");
        }
        Element sheet = root.elements("sheet").get(0);
        String res = sheet.asXML();
        // 标记key替换
        res = res.replaceAll("marker-refs","markerRefs").replaceAll("marker-ref","markerRef").
                replaceAll("marker-id","markerId");
        // 备注key替换
        res = res.replaceAll("plain","content");
        // 超链接(网络、文件、主题)替换
        res = res.replaceAll("xlink:href","xlinkHref");

        JSONObject xmlJSONObj = XML.toJSONObject(res);
        JSONObject jsonObject = xmlJSONObj.getJSONObject("sheet");

        JSONObject rootTopicJson = jsonObject.getJSONObject("rootTopic");
        if(!rootTopicJson.getJSONObject("children").isEmpty()){
            // 需要转换的数据 有的时候xmind一个数据会认为是字符串或者对象  其实转换需要的是对象或者数组 如  notes、markerRefs
            handlerTopicJson(rootTopicJson);
        }

        //设置缩进
        return jsonObject.toString(4);
    }

    /**
     * 递归处理需要转换的数据
     * @param rootTopicJson
     */
    private static void handlerTopicJson(JSONObject rootTopicJson) {
        JSONObject children = rootTopicJson.getJSONObject("children");
        Object attached = children.get("attached");
        JSONArray attachedList = new JSONArray();
        if(attached instanceof JSONObject){
            attachedList.put(attached);
        } else {
            attachedList = (JSONArray) attached;
        }
        for (Object object:attachedList) {
            JSONObject attachedObj = (JSONObject) object;
            //图标内容存在单个是对象格式 多个是数组格式，统一转换成数组格式
            if(attachedObj.has("markerRefs")){
                JSONObject markerRefs = attachedObj.getJSONObject("markerRefs");
                Object markerRef = markerRefs.get("markerRef");
                if(markerRef instanceof JSONObject){
                    JSONObject markerRefJson = (JSONObject) markerRef;
                    JSONArray jsonArray = new JSONArray();
                    jsonArray.put(markerRef);
                    markerRefs.put("markerRef",jsonArray);
                    attachedObj.put("markerRefs",markerRefs);
                }
            }
            // 如果是代码导出的xmind 原xml文件的备注 在生成时没有HTML，导致xml转json的时候只有content字段会被认为notes的值是字符串
            if(attachedObj.has("notes")){
                Object notes = attachedObj.get("notes");
                if(!(notes instanceof JSONObject)){
                    JSONObject notesJson = new JSONObject();
                    notesJson.put("content",notesJson);
                    attachedObj.put("notes",notesJson);
                }
            }
            if(attachedObj.has("children") && !attachedObj.getJSONObject("children").isEmpty() && attachedObj.getJSONObject("children").has("attached")){
                handlerTopicJson(attachedObj);
            }
        }
    }
}
