package com.lst.xmind;


import com.alibaba.fastjson2.JSON;
import com.lst.xmind.entity.TopicText;
import com.lst.xmind.entity.XmindRoot;
import org.xmind.core.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * demo: http://blog.itpub.net/70001864/viewspace-2790266/
 * 生成xmind
 *
 */
public class CreateXmind {

    public static void main(String[] args) throws CoreException, IOException {
        String rootJson = "{\"id\":\"2gth83r47g6qcrp5pbdhos4rq7\",\"rootTopic\":{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"id\":\"293odg9fvv6rtg6k01rjeull5p\",\"title\":\"身份证核查\"},{\"id\":\"1h2ia3rsskvq56ui66rnjb5t06\",\"title\":\"本行卡+密码\"},{\"children\":{\"attached\":[{\"id\":\"3aoeh1g41t9i8hkbdafll1a1nu\",\"title\":\"后期从销售系统取数\"},{\"id\":\"1cahsrnrsvg1jgf45rv87fcdij\",\"title\":\"先从核心取\"}]},\"id\":\"787ceimh3ka6rp1qvlt1fh5r9m\",\"title\":\"检查是否首次风评\"},{\"children\":{\"attached\":[{\"id\":\"364b24gnf67np5jfnnch7csl4n\",\"title\":\"结果送核心\"}]},\"id\":\"26pincvj0pf0kdsdf5ge6bom7v\",\"title\":\"风评等级修改\"}]},\"id\":\"7ittl7frm5n0t3h2c17sld61q0\",\"labels\":[{\"label\":\"暂时不做\"}],\"title\":\"风险评估\"},{\"children\":{\"attached\":[{\"id\":\"3ben3j13ilc4ekirl5j9ro9lef\",\"title\":\"身份证核查\"},{\"id\":\"369jg1hlgukaqhajtjl478kggb\",\"title\":\"客户本行卡校验\"},{\"id\":\"6go9r9rd80bdifte97melir8do\",\"title\":\"只查询当前卡下的理财\"},{\"children\":{\"attached\":[{\"id\":\"4f4c6qoognnfrlck7vb4sj2ans\",\"title\":\"时间区间\"}]},\"id\":\"3h5ales1kmgt88s4ktuhjd2sso\",\"title\":\"查询条件\"}]},\"id\":\"65meregbklaf7941bbpu51fgm9\",\"title\":\"客户理财查询\"}]},\"id\":\"2vir1232qmf839vl9re0ghsvop\",\"labels\":[{\"label\":\"核心查询，后期迁移到销售系统\"}],\"title\":\"理财\",\"xlinkHref\":\"https://gitee.com/\"},{\"comments\":[{\"author\":\"Admin\",\"content\":\"测试批注\",\"creationTime\":1685068303121}],\"id\":\"3aebaiosf4gg40tsr3uc6p47gs\",\"markerRefs\":{\"markerRef\":[{\"markerId\":\"priority-1\"}]},\"title\":\"在售理财产品查询\"}]},\"id\":\"6jf20n2freier3dbrqt61mpklt\",\"markerRefs\":{\"markerRef\":[{\"markerId\":\"smiley-smile\"}]},\"notes\":{\"content\":\"测试备注\"},\"title\":\"投资理财\"},{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"id\":\"2cvtn67rbn71rapcoe3et2lcrj\",\"title\":\"身份证核查\"},{\"id\":\"3c09f2qqvhj511hqkc7rdcedbv\",\"title\":\"信用卡申请资料填写\"},{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"id\":\"5b2dlv1nfhc86on939tt0n0ffv\",\"title\":\"身份证正反面\"},{\"id\":\"79ut2akpss4ssqf8saetf4m5ao\",\"title\":\"纸质材料照片\"},{\"id\":\"6pc3nnnf9nsrolnlfqdigdqqd7\",\"title\":\"合影\"},{\"id\":\"2mv4rc6mv4hkriuovcmouc88fk\",\"title\":\"征信授权书\"}]},\"id\":\"0ho561utmkhrui6r3uit0060hr\",\"title\":\"照片\"},{\"children\":{\"attached\":[{\"id\":\"3dsrcjpk3vr0k75pm2cql7n0ss\",\"title\":\"“本人同意XXXX”\"}]},\"id\":\"0airbafkh65d3trdf78k62iolf\",\"title\":\"视频\"},{\"children\":{\"attached\":[{\"id\":\"5fj1fvisn4fuhqrdu1qviia641\",\"title\":\"定位\"},{\"id\":\"1a2it617vbrqrpapci9avgr0l2\",\"title\":\"时间\"}]},\"id\":\"560utv6drb9dm26tr75pve62cv\",\"title\":\"水印\"}]},\"id\":\"07nebckji9dqse1dha1q4h0149\",\"title\":\"影像采集\"},{\"id\":\"0522cjehob37ket4hs95u02h4b\",\"title\":\"征信查询\"}]},\"id\":\"6uo8204t44vkhkringre8qsg0u\",\"title\":\"无卡\"},{\"children\":{\"attached\":[{\"id\":\"1vbaf5uu5qaut3fujiqshq97bc\",\"title\":\"身份证核查\"},{\"id\":\"695ulmokduq7gnve12p0lc4644\",\"title\":\"影像采集\"},{\"id\":\"1au6l3r235u15eldopitjele93\",\"title\":\"征信查询\"}]},\"id\":\"5olrim9f7vme8ltrq581lgtv7j\",\"title\":\"有卡\"}]},\"id\":\"308ke0li5pvrcbrp2pgoomi6jv\",\"labels\":[{\"label\":\"前置-通联系统\"}],\"title\":\"分期\"},{\"children\":{\"attached\":[{\"id\":\"5ildjfo0dmqicb2sm63nruslif\",\"title\":\"身份证核查\"},{\"id\":\"3rrm7ddj01hch7ad5vol867ii2\",\"title\":\"刷卡\"},{\"id\":\"2cfrebe2f7e7jkmbl304er84ga\",\"title\":\"设置交易密码，查询密码\"},{\"id\":\"3prop1mg08shf7s30vqhfljd8n\",\"title\":\"签名\"}]},\"id\":\"618390eddtr8l3p2fjvd77gfq3\",\"title\":\"激活\"},{\"children\":{\"attached\":[{\"id\":\"6hos6s7g90h7f3l4bd66o7dm62\",\"title\":\"审批url跳转\"}]},\"id\":\"2mom238d76cc0bgapsmp9rdvmn\",\"title\":\"移动审批\"}]},\"id\":\"1hnq7mlhr1a5bkptbu2mhspln8\",\"markerRefs\":{\"markerRef\":[{\"markerId\":\"priority-1\"}]},\"title\":\"信用卡\"},{\"children\":{\"attached\":[{\"id\":\"5ip38dnmtf7t3kemos5cntk71g\",\"title\":\"各系统是否与数据平台对接\"},{\"children\":{\"attached\":[{\"id\":\"75l3do44s1ssu887978vro3cp5\",\"title\":\"成功业务\"}]},\"id\":\"6m1tq6c8tbhn5thjm0uc9evn6j\",\"title\":\"日终取核心业务数据\"},{\"children\":{\"attached\":[{\"id\":\"54odoqjv49gihi87an5skk6ctr\",\"title\":\"所有的业务信息传到数据平台\"}]},\"id\":\"3v4eaa5mgrhb36ht7svgl5vgbm\",\"title\":\"如何取影像\"}]},\"id\":\"7jom1ca4clqamb5lqsl6ftrdkd\",\"title\":\"后督\"},{\"children\":{\"attached\":[{\"id\":\"32kcm95vtmb0lcjdn98h0h7e0v\",\"title\":\"上传影像后生成ID，根据ID取照片\"},{\"id\":\"7k9h036716v12t04alcv12s808\",\"title\":\"不存视频\"}]},\"id\":\"1kv9mrk6ujcctqc1srdnt6mf3a\",\"title\":\"影像平台\"},{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"id\":\"2f1qa85lv4un82l8mp6g81ut53\",\"title\":\"各系统用户独立\"},{\"children\":{\"attached\":[{\"id\":\"10a28j24k5qp5s7hlmqsd823ak\",\"title\":\"每日同步\"},{\"id\":\"6ucs6o0n2vv10q2hb7gdqf3nkl\",\"title\":\"首次全量，后面增量\"}]},\"id\":\"0rii6v8lu0l9r4h0anhfl1rvgs\",\"title\":\"以核心为准\"},{\"id\":\"5bnd4c5gb1pc951pcjt094lg8o\",\"title\":\"签退状态才能做业务\"},{\"id\":\"2q5rkkg8g83f3mr73ue9unga1m\",\"title\":\"mmc做开关，控制柜员登录时段，主管审批通过\"}]},\"id\":\"4k2enipmaac4qec7a6eh2tr57m\",\"title\":\"登录\"},{\"id\":\"2b44bdl24bukl9jb9v7rb2bio9\",\"title\":\"联网核查\"},{\"id\":\"38rj74p4gbo46kmki4efo9pk81\",\"title\":\"电子签名\"},{\"id\":\"69gugl1k17lp8c3rucbrg5qed2\",\"title\":\"轧账\"},{\"children\":{\"attached\":[{\"id\":\"1r9c0limh49v3vkquenr6n1lc1\",\"title\":\"用户管理\"},{\"id\":\"1ph0lfa9dbjvko9vm6qg3vm848\",\"title\":\"角色管理\"}]},\"id\":\"4kn4psvc0daa1t4rop690boq6o\",\"title\":\"mmc\"}]},\"id\":\"4mekdumuaglv20i5q89it8qs7p\",\"title\":\"基础功能\"},{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"id\":\"0lorj2451vm8kj817h16fga8pt\",\"title\":\"开卡同时发到e卡通公司\"},{\"id\":\"0gmjned781q84db5utdo5fic0b\",\"title\":\"新客户需要新建客户号，要录入客户信息\"},{\"id\":\"5nplpejci43864dh2u2j8jb4ai\",\"title\":\"刷身份证\"},{\"id\":\"6irkpvin3gjl3j0hu05t0et3it\",\"title\":\"刷卡\"},{\"id\":\"2uc5r62vbgdqgpc3trvicobrlt\",\"title\":\"存量客户校验\"},{\"id\":\"7re5ed9tmsschr0jlr9of9j725\",\"title\":\"信息采集\"},{\"id\":\"2oujmuj0i30id4kb6kgl10sp7c\",\"title\":\"影像采集\"},{\"id\":\"09m23n1tm4ldmrfl5usa7jm01f\",\"title\":\"短信验证，签名\"}]},\"id\":\"0gnira177b8o5l6124vupkuklk\",\"title\":\"开卡\"},{\"children\":{\"attached\":[{\"id\":\"2u0loobe7f5a8smk1fi0ful2bu\",\"title\":\"从本卡的借记账户转到钱包账户\"},{\"id\":\"436ot6hge581dtdq5hjipd04dl\",\"title\":\"刷身份证\"},{\"id\":\"3tp8bjn6tqatpunlatoo8agtnv\",\"title\":\"刷卡\"},{\"id\":\"7eru600s9qba07stdsjucu8cdl\",\"title\":\"圈存\"}]},\"id\":\"0t3b0v632oa6nlvbsurrmar6vr\",\"title\":\"圈存\"},{\"children\":{\"attached\":[{\"id\":\"3m93suim0h71700hmp3k87l6rn\",\"title\":\"刷身份证\"},{\"id\":\"2n7cdejmji9qv1rhjav0h94v0f\",\"title\":\"刷卡\"},{\"id\":\"5ohkrc1v534mcfeq4rcn6aanes\",\"title\":\"修改密码\"}]},\"id\":\"5sl74beoevmvqhqjlu0f96vh29\",\"title\":\"密码修改\"}]},\"id\":\"6fdc6q59in9nhpt6g2juoukaij\",\"title\":\"借记卡\"},{\"id\":\"3iq3uldneq0l44lpbef308epu0\",\"title\":\"atm转账撤销\"},{\"children\":{\"attached\":[{\"id\":\"75te4jbb9fhloac2ba8ks79sjn\",\"title\":\"ETC绑定签约\"},{\"id\":\"657v1roibrf75riguhnk0b8158\",\"title\":\"ETC预存\"},{\"id\":\"6fjmhvsqldv7bvgand25dr572t\",\"title\":\"ETC圈存\"}]},\"id\":\"57fmekg4a9q6hkram3ivb1ghpr\",\"title\":\"ETC业务\"}]},\"id\":\"13p5m6pi6qrb413f6ehnd08136\",\"title\":\"主题核心\"},\"title\":\"画布 1\"}\n";
        // xmind
        String xmindPath = "./export.xmind";
        createXmind(rootJson, xmindPath);
    }

    public static void createXmind(String rootJson, String path) throws CoreException, IOException {
        XmindRoot xmindRoot = JSON.parseObject(rootJson,XmindRoot.class);
        TopicText rootTopicData = xmindRoot.getRootTopic();

        // 创建思维导图的工作空间
        IWorkbookBuilder workbookBuilder = Core.getWorkbookBuilder();
        IWorkbook workbook = workbookBuilder.createWorkbook();
        // 获得默认sheet
        ISheet primarySheet = workbook.getPrimarySheet();
        // 给sheet设置标题
        primarySheet.setTitleText(xmindRoot.getTitle());
        // 获得根主题
        ITopic rootTopic = primarySheet.getRootTopic();
        // 设置根主题的标题
        rootTopic.setTitleText(rootTopicData.getTitle());
        //正确的逻辑图 org.xmind.ui.logic.right
        rootTopic.setStructureClass("org.xmind.ui.logic.right");
        //创建所有节点的集合
        ArrayList<ITopic> chapterTopics = new ArrayList<ITopic>();

        // 递归编辑将数据添加到根节点上
        if(rootTopicData.getChildren() != null && !rootTopicData.getChildren().getAttached().isEmpty()){
            for (TopicText topicText:rootTopicData.getChildren().getAttached()) {
                ITopic topic = workbook.createTopic();
                handlerTopic(topicText,topic,workbook);
                chapterTopics.add(topic);
            }
        }

        /*//创建一级主题
        ITopic topic = workbook.createTopic();
        //给主题设置标题名称
        topic.setTitleText("第一个，一级节点");
        chapterTopics.add(topic);
        //创建一级主题第二个
        ITopic topic1 = workbook.createTopic();
        //给主题设置标题名称
        topic1.setTitleText("第二个，一级节点");
        //父主题需要直接加到结合里面，后面会一起和跟节点关联
        chapterTopics.add(topic1);
        //设置二级主题，关联到第二个一级节点
        ITopic topicSecond = workbook.createTopic();
        topicSecond.setTitleText("二级节点");
        *//*二级节点是一级节点的子节点，所以可以直接进行关联（这里因为父节点刚加进去，所以坐标应该是size-1），无需add
        节点提供三种关联关系：
           String ATTACHED = "attached";
           String DETACHED = "detached";
           String SUMMARY = "summary";
         *//*
        chapterTopics.get(chapterTopics.size() - 1).add(topicSecond, ITopic.ATTACHED);
        //给二级节点设置笔记
        IPlainNotesContent plainContent = (IPlainNotesContent) workbook.createNotesContent(INotes.PLAIN);
        String content = "我是笔记内容";
        plainContent.setTextContent(content);
        INotes notes = topicSecond.getNotes();
        notes.setContent(INotes.PLAIN, plainContent);*/
        //把所有一级节点都加到根节点上
        chapterTopics.forEach(it -> rootTopic.add(it, ITopic.ATTACHED));
        workbook.save(path);
    }

    /**
     * 递归解析数据 进行创建主题
     * @param topicText json数据
     * @param topic 主题
     * @param workbook 创建思维导图的工作簿
     */
    private static void handlerTopic(TopicText topicText, ITopic topic, IWorkbook workbook) {
        // 给主题设置主题内容
        topic.setTitleText(topicText.getTitle());
        if(null != topicText.getNotes()){
            // 给节点设置笔记(备注)
            IPlainNotesContent plainNotesContent = (IPlainNotesContent) workbook.createNotesContent(INotes.PLAIN);
            plainNotesContent.setTextContent(topicText.getNotes().getContent());
            INotes notes = topic.getNotes();
            notes.setContent(INotes.PLAIN,plainNotesContent);
        }
        if(null != topicText.getComments() && !topicText.getComments().isEmpty()){
            // 批注 需要解析comments.xml
        }
        if(null != topicText.getMarkerRefs() && null != topicText.getMarkerRefs().getMarkerRef()){
            //图标
            topicText.getMarkerRefs().getMarkerRef().stream().forEach(e->{
                topic.addMarker(e.getMarkerId());
            });
        }
        if(null != topicText.getLabels() && !topicText.getLabels().isEmpty()){
            // 标签
            List<String> labels = topicText.getLabels().stream().map(e->e.getLabel()).collect(Collectors.toList());
            topic.setLabels(labels);
        }
        if(null != topicText.getXlinkHref()){
            // 超链接(网络、文件、主题)
            topic.setHyperlink(topicText.getXlinkHref());
        }
        // 有子节点
        if(null != topicText.getChildren() && !topicText.getChildren().getAttached().isEmpty()){
            for (TopicText topicChildText:topicText.getChildren().getAttached()) {
                ITopic topicChild = workbook.createTopic();
                /*二级节点是一级节点的子节点，所以可以直接进行关联（这里因为父节点刚加进去，所以坐标应该是size-1），无需add
                节点提供三种关联关系：
                   String ATTACHED = "attached";
                   String DETACHED = "detached";
                   String SUMMARY = "summary";
                 */
                handlerTopic(topicChildText,topicChild,workbook);
                topic.add(topicChild,ITopic.ATTACHED);
            }
        }
    }
}
