package com.lst.xmind;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.JSONPath;
import me.codeleep.jsondiff.common.model.JsonCompareResult;
import me.codeleep.jsondiff.common.model.JsonComparedOption;
import me.codeleep.jsondiff.core.DefaultJsonDifference;

import java.util.*;
import java.util.stream.Collectors;

/**
 * https://blog.csdn.net/beishida123/article/details/90443212
 * https://www.inte.net/news/277227.html
 */
public class JsonCompareUtils {

    public static void main(String[] args) {
        String jsonStr = "{\"id\":\"dd105675-a30c-40b9-a23b-716226253f11\",\"rootTopic\":{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"id\":\"7j659vagbvukhetht5fifjsaj7\",\"title\":\"8 放款成功\"}]},\"id\":\"10btbshg7eiv9q593fagi5b9fc\",\"title\":\"6 输入借款金额合计\"}]},\"id\":\"0p86953rmadn3ictvt5sus6qc9\",\"title\":\"5 输入放款账号\"}]},\"id\":\"61qtni8fosnaj555cc20dhv2t2\",\"title\":\"4 输入借据序号\"}]},\"id\":\"5uqd4diodjt1vioqpe352338d0\",\"title\":\"3 输入合同号\"}]},\"id\":\"6j1abjqnis7ptq3j2nmeiqj355\",\"title\":\"2.1 综合前端002211\"},{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"id\":\"70s9390uqtc263jhk06rkto1q9\",\"title\":\"8 放款成功\"}]},\"id\":\"17gurq3eoj166ma6bo7b79tchq\",\"title\":\"7 选择需复核的记录\"}]},\"id\":\"6itsk58d8vtivsfgiltj08ufol\",\"title\":\"2.2 个人网银\"},{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"id\":\"37aklq1nl9bm05dak088rbaaas\",\"title\":\"8 放款成功\"}]},\"id\":\"5p6kmbejdkimoq3b5abdkvgi0l\",\"title\":\"7 选择需复核的记录\"}]},\"id\":\"3c8br3e5jnuh08t3h37val0j41\",\"title\":\"2.3 丰收互联\"}]},\"id\":\"40johtk4jnvt983oughvatnfre\",\"title\":\"前提条件：客户已生成循环贷款借据\"},{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"id\":\"05lqd4n3ce3famlui9ato39de3\",\"title\":\"8 放款成功\"}]},\"id\":\"02m73q2pvqobtak9i4a72dlat0\",\"title\":\"6 输入借款金额合计\"}]},\"id\":\"47fkfjn5u86p7n0946pf3i5uf7\",\"title\":\"5 输入放款账号\"}]},\"id\":\"1jjgsqf7h1dg5r3dnvbq9jv061\",\"title\":\"4 输入借据序号\"}]},\"id\":\"59eaqkit2dqiossn2kc3edufsf\",\"title\":\"3 输入合同号\"}]},\"id\":\"77edoa7cnim5351g3oqvtcsg7t\",\"title\":\"2.4 综合前端002212\"}]},\"id\":\"788991fvfqteou9vi6rcodr0fm\",\"title\":\"前提条件：客户已生成普通贷款借据\"}]},\"id\":\"50odf86jir1psukvopes1imtqs\",\"title\":\"对私:非分期贷款放款流程\"},{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"id\":\"6l5drhmespp1ms73qe5t03a0b5\",\"title\":\"8 放款成功\"}]},\"id\":\"7crlld57dek1a5p191ju0dumek\",\"title\":\"6 输入借款金额合计\"}]},\"id\":\"1i1p58n9ca6kiorrnlncg7a6nh\",\"title\":\"5 输入放款账号\"}]},\"id\":\"02s0k8ife6fdbcmjeio8blivvc\",\"title\":\"4 输入借据序号\"}]},\"id\":\"0fmml3mfks3k4gofu5kompr3pl\",\"title\":\"3 输入合同号\"}]},\"id\":\"24f1tkc63h67t0jh0hk635vhvn\",\"title\":\"2.5 综合前端002221\"}]},\"id\":\"6u6btengjnkbmrd13p6lmaft4u\",\"title\":\"前提条件：客户已生成分期贷款借据\"}]},\"id\":\"1ju5nb0ghp6rvs7q1nphablkv6\",\"title\":\"对私:分期贷款放款流程\"},{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"id\":\"7i0eksofqhoce5ecn6r16jrvct\",\"title\":\"8 放款成功\"}]},\"id\":\"7bd2gfddd0cpcubiketdb2v2fb\",\"title\":\"6 输入借款金额合计\"}]},\"id\":\"6obpulk5jl571pudovl8fcd40g\",\"title\":\"5 输入放款账号\"}]},\"id\":\"3qp3cf7rvsv5t02vfq0saj4drt\",\"title\":\"4 输入借据序号\"}]},\"id\":\"42eni62tvfitomeham7fq27nrs\",\"title\":\"3 输入合同号\"}]},\"id\":\"7j6dkk1cso59cc55stbj3o5bq0\",\"title\":\"2.1 综合前端002211\"},{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"id\":\"4db0u78m9vnirfn64opni9731j\",\"title\":\"8 放款成功\"}]},\"id\":\"71sct4rm0jbqa485hhs1o8v87t\",\"title\":\"7 选择需复核的记录\"}]},\"id\":\"1d5a7chictntj6plgj3m7mbtri\",\"title\":\"2.6 企业网银\"},{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"id\":\"7kkg2g6kc1enh5q7ghqq785j47\",\"title\":\"8 放款成功\"}]},\"id\":\"34dr50vm0at105hk4fumd2k1ng\",\"title\":\"7 选择需复核的记录\"}]},\"id\":\"1mt2u33r7bm47uj1shnb0k1dvp\",\"title\":\"2.7 企业互联\"}]},\"id\":\"5moc78dpdqi52pjmnntcrhi3if\",\"title\":\"前提条件：客户已生成循环贷款借据\"},{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"id\":\"7c9r48skf1sqqrkjm5renbv0om\",\"title\":\"8 放款成功\"}]},\"id\":\"4qgsen58o5auus2ip5j1nhgm4t\",\"title\":\"6 输入借款金额合计\"}]},\"id\":\"58g6g4ktsedt7dkcs5t1sr6frq\",\"title\":\"5 输入放款账号\"}]},\"id\":\"4j9nkfqpobnusps9eivkk0es5u\",\"title\":\"4 输入借据序号\"}]},\"id\":\"09om7mhter0oos9epq8tfs637s\",\"title\":\"3 输入合同号\"}]},\"id\":\"27b2dcfci7et8djnoc9uc76fv9\",\"title\":\"2.4 综合前端002212\"}]},\"id\":\"5kvlnd1b5bjnpk2abut0k5gbns\",\"title\":\"前提条件：客户已生成普通贷款借据\"}]},\"id\":\"6t2l82dd3v8r0m5sci6h1voo1s\",\"title\":\"对公:非分期贷款放款流程\"},{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"id\":\"625hhd6o94ntmm2j7p3un6kaeg\",\"title\":\"8 放款成功\"}]},\"id\":\"6vue8fl7mahmr51vfsjrfmipab\",\"title\":\"6 输入借款金额合计\"}]},\"id\":\"3mgujlodrks562c90vheh5ncpu\",\"title\":\"5 输入放款账号\"}]},\"id\":\"4tttl0v47cnk7dmtm33u911p5p\",\"title\":\"4 输入借据序号\"}]},\"id\":\"7ma3onmqar8uj9nhk7r03u18ft\",\"title\":\"3 输入合同号\"}]},\"id\":\"48min3f3g5idr094egj6j6c5nl\",\"title\":\"2.5 综合前端002221\"}]},\"id\":\"6s94isjfcqt3npqd99n8kr8fjq\",\"title\":\"前提条件：客户已生成分期贷款借据\"}]},\"id\":\"76hhc34idc28nctkra6b4dvebb\",\"title\":\"对公:分期贷款放款流程\"}]},\"id\":\"afb345b7-155b-4f7e-8b3b-bfc045e1c764\",\"title\":\"业务流程\"},{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"id\":\"5u10q4r3aesq719i2r8jk88o04\",\"title\":\"合同有效\"},{\"id\":\"35eajgnnjo0q18gm6pmpr9n2rt\",\"title\":\"合同非有效\"}]},\"id\":\"1osa5ute3i1vo8pe4a1cltg94l\",\"title\":\"合同状态检查\"}]},\"id\":\"2qug43s49n5l99v6dh1t28vl5i\",\"title\":\"3 输入合同号\"},{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"id\":\"e89f2f2a-ddd7-4baf-aa41-b312b8ed249c\",\"title\":\"借据有效\"},{\"id\":\"ea0bdbcf-98ba-4419-85d7-c51037c91793\",\"title\":\"借据非有效\"}]},\"id\":\"48ho3grm948uql5tu59p0eiv4e\",\"title\":\"借据状态校验\"}]},\"id\":\"0gnktu0cregv7ibqb9m81okcb5\",\"title\":\"4 输入借据序号\"},{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"id\":\"6893846f-27ff-4e67-9d11-4523b626cb7d\",\"title\":\"账户与借据录入账号不一致\"},{\"id\":\"9322820f-3fd1-491a-af9f-ebc59fd9c147\",\"title\":\"账户与借据录入账号一致\"}]},\"id\":\"9051e7e4-7628-4743-b64a-a1a9f3b320e6\",\"title\":\"账户一致性检查\"}]},\"id\":\"4q07nq88bkan76cvmlpb0gn7tt\",\"title\":\"5 输入放款账号\"},{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"id\":\"2uaf9gsjgm2su95jbuc5ltuds8\",\"title\":\"金额需等于借据金额\"}]},\"id\":\"4c468d76-5561-44d4-9b70-9b77a08a36a9\",\"title\":\"金额校验\"}]},\"id\":\"915ec08f-25a5-4899-852c-34208f9fdab1\",\"title\":\"6 输入借款金额合计\"}]},\"id\":\"be177004-fe29-405e-9f6e-5dcf08f7d3ec\",\"title\":\"业务规则\"},{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"id\":\"6ado07ngnk2dba1tpcs7013c40\",\"title\":\"进入下一页\"}]},\"id\":\"3r4kmttr9lp0dobn21vtmpk5i6\",\"title\":\"确认按钮\"}]},\"id\":\"3rn11a8ehn7cekvpbo8ad24po1\",\"title\":\"合同号\"},{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"id\":\"0p8qlrd5il0k6de7p2ql6eh77l\",\"title\":\"进入下一页\"}]},\"id\":\"1clf91q28d62r3oburrskkj7j5\",\"title\":\"确认按钮\"}]},\"id\":\"2eqrc7l3drlah0a08pb0gdiene\",\"title\":\"借据序号\"},{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"id\":\"2qh2ceiej6optksc2lln2s6dq4\",\"title\":\"进入下一页\"}]},\"id\":\"22ldo33mgs08a8k1jt34aagepd\",\"title\":\"确认按钮\"}]},\"id\":\"59nud7het7hvd093nvn4uk0mog\",\"title\":\"放款账号\"},{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"id\":\"04aggiuea0h0npq0pch7m8cgu6\",\"title\":\"进入下一页\"}]},\"id\":\"4rbtrtholkqckdhp9qu4l4rc6f\",\"title\":\"确认按钮\"}]},\"id\":\"2icm2gmsp4h3eevqipfbsqhqqq\",\"title\":\"放款金额\"}]},\"id\":\"1gjuvq0k6992d90rkon7l5v7v4\",\"title\":\"输入\"},{\"children\":{\"attached\":[{\"id\":\"07ur1p1q97i5b3j9o3ff41srnj\",\"title\":\"客户号、客户名称、产品名称、担保方式、币种、利率、借款日期、到期日期、放款方式、自动还款方式、结息方式、复利方式等\"}]},\"id\":\"313r4h6rl3oc3aqhl6142ablmk\",\"title\":\"输出\"}]},\"id\":\"34pl5gh2jviaepektuhvbuv7f6\",\"title\":\"业务申请页面\"},{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"id\":\"2vj57ibto1tpktgbh42sin79vm\",\"title\":\"进入下一页\"}]},\"id\":\"4p002b57s65sopffp65ujidsde\",\"title\":\"点击确认\"},{\"children\":{\"attached\":[{\"id\":\"7r92if6bdo6jhkob95fnfepksn\",\"title\":\"返回上一页\"}]},\"id\":\"10dekfpj56fu0t30gd7g77so9l\",\"title\":\"点击取消\"}]},\"id\":\"5mc3vfsp98jn2prolql3h263j8\",\"title\":\"确认、取消\"}]},\"id\":\"7sefdkdma1kuubs135u4po5o6i\",\"title\":\"确认交易信息\"},{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"id\":\"2hpm0ea2sr2nrppjk6nkg41aq5\",\"title\":\"关闭验签弹框并进入放款成功页\"}]},\"id\":\"3q4jglf6iac1g3ighcc8vimheg\",\"title\":\"验签确认\"},{\"children\":{\"attached\":[{\"id\":\"00nvertqhhlce67nod06g07r98\",\"title\":\"关闭验签弹框\"}]},\"id\":\"3in03i8dmsasej6mdpbekoijlh\",\"title\":\"验签取消\"}]},\"id\":\"6a34ouq57tg5dfo5jtl7g1ilqv\",\"title\":\"点击弹出验签框\"}]},\"id\":\"48i65imed40ev1beckk0f04pgd\",\"title\":\"验签\"},{\"children\":{\"attached\":[{\"id\":\"3oi38rhafueqto32bngqerohfs\",\"title\":\"点击返回主界面\"}]},\"id\":\"4g4g2phldscgtsf20q3hr59r3m\",\"title\":\"取消交易\"},{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"id\":\"48c9ugqctfres3g5mt7644r496\",\"title\":\"进入下一页\"}]},\"id\":\"1i6thrnl5ofe7jv14ar7kgsqvt\",\"title\":\"点击确认\"}]},\"id\":\"6tkdo7uoie3jfhphpes5f27t70\",\"title\":\"复核\"}]},\"id\":\"138t6qlocjonq0e2gqqcb10u2p\",\"title\":\"复核办理业务并验签\"}]},\"id\":\"4adhss13guard04sik2egs9jcd\",\"title\":\"输入\"},{\"children\":{\"attached\":[{\"id\":\"6sh3blv70r9bc364p7atujhet6\",\"title\":\"客户号、客户名称、利率、借款日期、到期日期等信息\"}]},\"id\":\"37cot198uufn2rn3fj0qktvss5\",\"title\":\"输出\"}]},\"id\":\"3qvckkdn662n54dtjtikg1p0jp\",\"title\":\"复核页面\"},{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"id\":\"4aerstek8neniktrjapher0qv9\",\"title\":\"点击打印凭条\"}]},\"id\":\"4pduq206hkm9nemu9uqc44bgqu\",\"title\":\"打印\"},{\"children\":{\"attached\":[{\"id\":\"51vcl4lktm0sb5f0m0ng6qiocf\",\"title\":\"点击返回主页面，不打印凭条\"}]},\"id\":\"4g1lop3fr1urni0p68oba2qj17\",\"title\":\"不打印\"}]},\"id\":\"71d5mn1bmnak1n45ftolc43nl2\",\"title\":\"输入\"},{\"children\":{\"attached\":[{\"id\":\"6glli5v98sqtffqumf2mq4dopu\",\"title\":\"提示：交易成功，是否打印凭条\"}]},\"id\":\"592dlp0hf4sjl051kbkllj5gk9\",\"title\":\"输出\"}]},\"id\":\"5fqcm4crh9i4op0t6pkvu74i65\",\"title\":\"交易结果页面\"}]},\"id\":\"1keksbe721km94lb2ntheioreg\",\"title\":\"页面控制\"}]},\"id\":\"b9aa22deba98b3b20c7ac8aca2\",\"title\":\"贷款放款分析\"},\"title\":\"画布 1\"}";
        String jsonStr2 = "{\"id\":\"dd105675-a30c-40b9-a23b-716226253f11\",\"rootTopic\":{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"id\":\"5vf0mf57g437kmf70mlqesjuag\",\"title\":\"11 还款成功\"}]},\"id\":\"3sae7vpj1cfv9u4bmragsjing1\",\"title\":\"10 输入还款金额\"}]},\"id\":\"7stspgirf4ka6aabmvl4fn4oq2\",\"title\":\"7.1 选择还款内容：还本\"},{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"id\":\"4r5s8rkcva73eibrotahb31v4i\",\"title\":\"11 还款成功\"}]},\"id\":\"0kg54arnbl9c08lqbrb0troak9\",\"title\":\"10 输入还款金额\"}]},\"id\":\"0geifsfgqlonokuetutpcntlvs\",\"title\":\"7.2 选择还款内容：还息\"},{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"id\":\"3fmltpud40b3anjckq6d1b5j8f\",\"title\":\"11 还款成功\"}]},\"id\":\"0iidp6vb5i56f34h6i3hmchm2n\",\"title\":\"10 输入还款金额\"}]},\"id\":\"54ca2qtt3b53aeh7kekdqtll5f\",\"title\":\"7.3 选择还款内容：还本付息\"},{\"children\":{\"attached\":[{\"id\":\"2hs4iok4fthhsj5ni6ej4moufu\",\"title\":\"11 还款成功\"}]},\"id\":\"73jrtgeccotlsp4l2ehfjn7udi\",\"title\":\"7.4 选择还款内容：销户\"}]},\"id\":\"3pholl3krfavigfgsisu53ep38\",\"title\":\"6 输入还贷息凭证号\"}]},\"id\":\"7m5nj0oerbe29is48el7gnfhlg\",\"title\":\"4 输入借据序号\"}]},\"id\":\"2mt29r7dpaovc8s6opu60dorn1\",\"title\":\"3 输入合同号\"}]},\"id\":\"4pnthn9q51lt3l0g88l3rv821m\",\"title\":\"2.1 综合前端002213\"},{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"id\":\"4u8nu0cg918m7ooh1k0rh7daas\",\"title\":\"11 还款成功\"}]},\"id\":\"1vgdfetat3th03ajm5gfpl9h9m\",\"title\":\"9.1 选择还款方式：全额还款\"},{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"id\":\"6or61nelt54vdqsqg386bkc8us\",\"title\":\"11 还款成功\"}]},\"id\":\"4jtvrvtmjrsa9q9hv9b651dp0m\",\"title\":\"10 输入还款金额\"}]},\"id\":\"2i6dctvbaanko54cmo6i3mbfo3\",\"title\":\"9.2 选择还款方式：部分还款\"}]},\"id\":\"4kppd3o1vntsmhni8cjghnpfm7\",\"title\":\"8 选择需还款的借据\"}]},\"id\":\"05l8v3adcvetgb46qpda3h7irh\",\"title\":\"2.2 个人网银\"},{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"id\":\"4h4ocl3egbeidkdlumsfnk1rqj\",\"title\":\"11 还款成功\"}]},\"id\":\"1umpqtvt1aggphsd0mrdguq2in\",\"title\":\"9.1 选择还款方式：全额还款\"},{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"id\":\"1h5lh8og1o5u9t14cb0tagsk3r\",\"title\":\"11 还款成功\"}]},\"id\":\"2i26pl2anrbb3e7e4d31odrvd9\",\"title\":\"10 输入还款金额\"}]},\"id\":\"1p8fn0m084hphstm1bisd1dc3q\",\"title\":\"9.2 选择还款方式：部分还款\"}]},\"id\":\"1mp0pesl43l3one6ifm3qgnqv9\",\"title\":\"8 选择需还款的借据\"}]},\"id\":\"3onivfgp2kll7frdcb5h9kh3ef\",\"title\":\"2.3 丰收互联\"}]},\"id\":\"03e3vkb3r590vet41e0olut8ka\",\"title\":\"前提条件：贷款已放款成功\"}]},\"id\":\"77lppmbqh4oi5i7p69df3hjbd4\",\"title\":\"对私:非分期贷款还款流程\"},{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"id\":\"27dnupubcfg2cucboprs6nurm7\",\"title\":\"11 还款成功\"}]},\"id\":\"55ikuavjpqlcoqki8n2qrsf0jq\",\"title\":\"6 输入收贷息凭证号\"}]},\"id\":\"0pkehs0sn2m26kr6i8icfprnc8\",\"title\":\"10 输入还款金额\"}]},\"id\":\"1u5nl6ahqvch25ulebeablcdg7\",\"title\":\"5 输入还款期数\"}]},\"id\":\"7m3kjent5edebuhgj99v4eg8ko\",\"title\":\"4 输入借据序号\"}]},\"id\":\"3k38m3freshn7oo36iu6mhvij4\",\"title\":\"3 输入合同号\"}]},\"id\":\"6hkq2urpij9rp83s3esrh3mhh3\",\"title\":\"2.4 综合前端002223\"}]},\"id\":\"0r2lv7ljesgvva1a81f2do64m5\",\"title\":\"前提条件：分期贷款已放款成功\"}]},\"id\":\"6umc73bjkedel6o9dsn4ol903f\",\"title\":\"对私:分期贷款还款流程\"},{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"id\":\"7ko6r1hs5kqdbjptq4s9ij1tj6\",\"title\":\"11 还款成功\"}]},\"id\":\"772vlue5bvi3o8o5acnvjni5oi\",\"title\":\"10 输入还款金额\"}]},\"id\":\"72n2hthnbf87bt298ri7lfma38\",\"title\":\"7.1 选择还款内容：还本\"},{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"id\":\"1ahet302ii99f4n0gn05epc3am\",\"title\":\"11 还款成功\"}]},\"id\":\"7fv8bb5pq4qbbag3bhr5re6dm2\",\"title\":\"10 输入还款金额\"}]},\"id\":\"47dn0o8fegcg4e3mv2qjh76pdc\",\"title\":\"7.2 选择还款内容：还息\"},{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"id\":\"4ac6b4jkqst33755ggpm1kbs8o\",\"title\":\"11 还款成功\"}]},\"id\":\"0ba0p7krjir1h3i57ehds6dkd4\",\"title\":\"10 输入还款金额\"}]},\"id\":\"0eob33urfj9ds4mstv6336ss6m\",\"title\":\"7.3 选择还款内容：还本付息\"},{\"children\":{\"attached\":[{\"id\":\"3c2dif31revg1ll0vq1e8tatmq\",\"title\":\"11 还款成功\"}]},\"id\":\"4oo5u54uhsvii9hhnln7lgsfgl\",\"title\":\"7.4 选择还款内容：销户\"}]},\"id\":\"4gakqejcghhndbbilepulgfcno\",\"title\":\"6 输入还贷息凭证号\"}]},\"id\":\"7736sgu5h35qqdi7n7r7jn5154\",\"title\":\"4 输入借据序号\"}]},\"id\":\"0117fismirjatgtumba5jfkk8o\",\"title\":\"3 输入合同号\"}]},\"id\":\"1tidv1503sg40d56ihnj4d7va9\",\"title\":\"2.1 综合前端002213\"},{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"id\":\"3eivmmrqrbq5na24u40u65m4o0\",\"title\":\"11 还款成功\"}]},\"id\":\"67eece74mnk37ol2256lkm6dbp\",\"title\":\"9.1 选择还款方式：全额还款\"},{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"id\":\"5q437did7up3k7t2ffpsn641tl\",\"title\":\"11 还款成功\"}]},\"id\":\"5qb0ngedu8fat4jaiqu1jto2ho\",\"title\":\"10 输入还款金额\"}]},\"id\":\"7i2shq499qrofqin7h5d3lkv5t\",\"title\":\"9.2 选择还款方式：部分还款\"}]},\"id\":\"1hgpe298g5uq9iio3avq5h3o1e\",\"title\":\"8 选择需还款的借据\"}]},\"id\":\"0jhojs6mh5ajlso4c50oeeidmj\",\"title\":\"2.5 企业网银\"},{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"id\":\"16dfbl8bii63rpssdrf641kn06\",\"title\":\"11 还款成功\"}]},\"id\":\"7h86nh5d8ubmcfpqot8lhmj9rd\",\"title\":\"9.1 选择还款方式：全额还款\"},{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"id\":\"4gsk05bqnfd2vbqjjksaaiuh4u\",\"title\":\"11 还款成功\"}]},\"id\":\"1h30k7qumqfi7aatudm214qojr\",\"title\":\"10 输入还款金额\"}]},\"id\":\"20su157cmva3lpur49dnlme2ns\",\"title\":\"9.2 选择还款方式：部分还款\"}]},\"id\":\"4kmtfvr771lk3ei210q1u9tf1e\",\"title\":\"8 选择需还款的借据\"}]},\"id\":\"47c2duefvbgdu4g464786j6nqg\",\"title\":\"2.6 企业互联\"}]},\"id\":\"7itk9q4u9tblbtahrdrk3ifbvj\",\"title\":\"前提条件：贷款已放款成功\"}]},\"id\":\"199hf9juinvaogfh24ok92gnnr\",\"title\":\"对公:非分期贷款放款流程\"},{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"id\":\"6i7ehvgrsv0jq60n7meej28s9f\",\"title\":\"11 还款成功\"}]},\"id\":\"0j5goeh7r6tm2b7avcvjdpserp\",\"title\":\"6 输入收贷息凭证号\"}]},\"id\":\"4813ij3hpbf9mp0fhms4274boe\",\"title\":\"10 输入还款金额\"}]},\"id\":\"0mcvuet7b8oprgc0qgi661k6q0\",\"title\":\"5 输入还款期数\"}]},\"id\":\"6aul3ld7psn1qlajfpfbnf1n19\",\"title\":\"4 输入借据序号\"}]},\"id\":\"7f01cna05fhpkmpscfh9r6njs8\",\"title\":\"3 输入合同号\"}]},\"id\":\"3j1t4mfl79io19vv7jpdchnrgn\",\"title\":\"2.4 综合前端002223\"}]},\"id\":\"3rm44h4vqqiulqf2ulb3rj12m1\",\"title\":\"前提条件：分期贷款已放款成功\"}]},\"id\":\"5sn43tdqtie4rlbjsekqg8nf35\",\"title\":\"对公:分期贷款放款流程\"}]},\"id\":\"afb345b7-155b-4f7e-8b3b-bfc045e1c764\",\"title\":\"业务流程\"},{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"id\":\"5u10q4r3aesq719i2r8jk88o04\",\"title\":\"合同有效\"},{\"id\":\"35eajgnnjo0q18gm6pmpr9n2rt\",\"title\":\"合同非有效\"}]},\"id\":\"1osa5ute3i1vo8pe4a1cltg94l\",\"title\":\"合同状态检查\"}]},\"id\":\"2qug43s49n5l99v6dh1t28vl5i\",\"title\":\"3 输入合同号\"},{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"id\":\"e89f2f2a-ddd7-4baf-aa41-b312b8ed249c\",\"title\":\"借据有效\"},{\"id\":\"ea0bdbcf-98ba-4419-85d7-c51037c91793\",\"title\":\"借据非有效\"}]},\"id\":\"48ho3grm948uql5tu59p0eiv4e\",\"title\":\"借据状态校验\"}]},\"id\":\"0gnktu0cregv7ibqb9m81okcb5\",\"title\":\"4 输入借据序号\"},{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"id\":\"7r0mn3gontlhi4bjlcia2amsek\",\"title\":\"期数大于合同约定期数\"},{\"id\":\"4m394q67ikjb70evlt8phlac0u\",\"title\":\"期数小于等于合同约定期数\"},{\"id\":\"63704k2pe7srkku961rk2afscn\",\"title\":\"期数输入为0时，可直接输入小于等于合同金额的任意还款金额\"}]},\"id\":\"0tncjci09r137j56l2urald9t0\",\"title\":\"期数大小检查\"}]},\"id\":\"53vnqfbnq0lb0b6eat523au64q\",\"title\":\"5 输入还款期数\"},{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"id\":\"6893846f-27ff-4e67-9d11-4523b626cb7d\",\"title\":\"凭证号非该凭证最小凭证号\"},{\"id\":\"9322820f-3fd1-491a-af9f-ebc59fd9c147\",\"title\":\"凭证号为该凭证最小凭证号\"}]},\"id\":\"9051e7e4-7628-4743-b64a-a1a9f3b320e6\",\"title\":\"凭证号校验\"}]},\"id\":\"4q07nq88bkan76cvmlpb0gn7tt\",\"title\":\"6 输入还贷息凭证号\"},{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"id\":\"2uaf9gsjgm2su95jbuc5ltuds8\",\"title\":\"金额需小于借据金额\"}]},\"id\":\"4c468d76-5561-44d4-9b70-9b77a08a36a9\",\"title\":\"金额校验\"}]},\"id\":\"915ec08f-25a5-4899-852c-34208f9fdab1\",\"title\":\"10 输入还款金额\"}]},\"id\":\"be177004-fe29-405e-9f6e-5dcf08f7d3ec\",\"title\":\"业务规则\"},{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"id\":\"6ado07ngnk2dba1tpcs7013c40\",\"title\":\"进入下一页\"}]},\"id\":\"3r4kmttr9lp0dobn21vtmpk5i6\",\"title\":\"确认按钮\"}]},\"id\":\"3rn11a8ehn7cekvpbo8ad24po1\",\"title\":\"合同号\"},{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"id\":\"0p8qlrd5il0k6de7p2ql6eh77l\",\"title\":\"进入下一页\"}]},\"id\":\"1clf91q28d62r3oburrskkj7j5\",\"title\":\"确认按钮\"}]},\"id\":\"2eqrc7l3drlah0a08pb0gdiene\",\"title\":\"借据序号\"},{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"id\":\"2qh2ceiej6optksc2lln2s6dq4\",\"title\":\"进入下一页\"}]},\"id\":\"22ldo33mgs08a8k1jt34aagepd\",\"title\":\"确认按钮\"}]},\"id\":\"59nud7het7hvd093nvn4uk0mog\",\"title\":\"还贷息凭证号\"},{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"id\":\"04aggiuea0h0npq0pch7m8cgu6\",\"title\":\"进入下一页\"}]},\"id\":\"4rbtrtholkqckdhp9qu4l4rc6f\",\"title\":\"确认按钮\"}]},\"id\":\"2icm2gmsp4h3eevqipfbsqhqqq\",\"title\":\"还款金额\"}]},\"id\":\"1gjuvq0k6992d90rkon7l5v7v4\",\"title\":\"输入\"},{\"children\":{\"attached\":[{\"id\":\"07ur1p1q97i5b3j9o3ff41srnj\",\"title\":\"客户号、客户名称、产品名称、担保方式、币种、利率、借款日期、到期日期、放款方式、自动还款方式、结息方式、复利方式等\"}]},\"id\":\"313r4h6rl3oc3aqhl6142ablmk\",\"title\":\"输出\"}]},\"id\":\"34pl5gh2jviaepektuhvbuv7f6\",\"title\":\"业务申请页面\"},{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"children\":{\"attached\":[{\"id\":\"4aerstek8neniktrjapher0qv9\",\"title\":\"点击打印凭条\"}]},\"id\":\"4pduq206hkm9nemu9uqc44bgqu\",\"title\":\"打印\"},{\"children\":{\"attached\":[{\"id\":\"51vcl4lktm0sb5f0m0ng6qiocf\",\"title\":\"点击返回主页面，不打印凭条\"}]},\"id\":\"4g1lop3fr1urni0p68oba2qj17\",\"title\":\"不打印\"}]},\"id\":\"71d5mn1bmnak1n45ftolc43nl2\",\"title\":\"输入\"},{\"children\":{\"attached\":[{\"id\":\"6glli5v98sqtffqumf2mq4dopu\",\"title\":\"提示：交易成功，是否打印凭条\"}]},\"id\":\"592dlp0hf4sjl051kbkllj5gk9\",\"title\":\"输出\"}]},\"id\":\"5fqcm4crh9i4op0t6pkvu74i65\",\"title\":\"交易结果页面\"}]},\"id\":\"1keksbe721km94lb2ntheioreg\",\"title\":\"页面控制\"}]},\"id\":\"b9aa22deba98b3b20c7ac8aca2\",\"title\":\"贷款还款分析\"},\"title\":\"画布 1\"}";
        /*JsonComparedOption jsonComparedOption = new JsonComparedOption().setIgnoreOrder(true);
        JsonCompareResult jsonCompareResult = new DefaultJsonDifference()
                .option(jsonComparedOption)
                .detectDiff(JSON.parseObject(jsonStr), JSON.parseObject(jsonStr2));
        System.out.println(JSON.toJSONString(jsonCompareResult));*/

       /* Set sameJsonPath = getTwoJsonSameJsonPath(JSON.parseObject(jsonStr),JSON.parseObject(jsonStr2));
        checkTwoJsonSame(sameJsonPath,JSON.parseObject(jsonStr),JSON.parseObject(jsonStr2));*/


        Set allJsonPath = getTwoJsonAllJsonPath(JSON.parseObject(jsonStr),JSON.parseObject(jsonStr2));
        checkTwoJsonSame(allJsonPath,JSON.parseObject(jsonStr),JSON.parseObject(jsonStr2));
    }



    /**
     * @param jsonObject
     * @return
     */
    public static List<String> getListJson(JSONObject jsonObject) {
        // 获取到所有jsonpath后，获取所有的key
        List<String> jsonPaths = JSONPath.paths(jsonObject).keySet().stream().collect(Collectors.toList());
        //去掉根节点key
        List<String> parentNode = new ArrayList<>();
        //根节点key
        parentNode.add("/");
        //循环获取父节点key，只保留叶子节点
        for (int i = 0; i < jsonPaths.size(); i++) {
            if (jsonPaths.get(i).lastIndexOf("/") > 0) {
                parentNode.add(jsonPaths.get(i).substring(0, jsonPaths.get(i).lastIndexOf("/")));
            }
        }

        //remove父节点key
        for (String parentNodeJsonPath : parentNode) {
            jsonPaths.remove(parentNodeJsonPath);
        }

        List<String> jsonPathList = new ArrayList<>();
        Iterator<String> jsonPath = jsonPaths.iterator();
        //将/替换为点.
        while (jsonPath.hasNext()) {
            jsonPathList.add(jsonPath.next().replaceFirst("/", "").replaceAll("/", "."));
        }
        //排序
        Collections.sort(jsonPathList);
        return jsonPathList;
    }

    /**
     * 获取两个Json相同的JsonPath
     * @param oneJson
     * @param twoJson
     * @return
     */
    public static Set getTwoJsonSameJsonPath(JSONObject oneJson, JSONObject twoJson){
        List<String> oneListJsonPath = getListJson(oneJson);
        List<String> twoListJsonPath = getListJson(twoJson);
        //获取两个json有相同路径的jsonPath
        Set sameJsonPath = new HashSet();
        for (String oneJsonPath:oneListJsonPath){
            for (String twoJsonPath:twoListJsonPath){
                if (oneJsonPath.equals(twoJsonPath)){
                    sameJsonPath.add(oneJsonPath);
                    break;
                }
            }
        }
        return sameJsonPath;
    }


    /**
     * 获取两个json所有的jsonPath
     * @param oneJson
     * @param twoJson
     * @return
     */
    public static  Set getTwoJsonAllJsonPath(JSONObject oneJson,JSONObject twoJson){
        List<String> oneListJsonPath = getListJson(oneJson);
        List<String> twoListJsonPath = getListJson(twoJson);
        //获取两个json有相同路径的jsonPath
        Set allJsonPath = new HashSet();
        allJsonPath.addAll(oneListJsonPath);
        allJsonPath.addAll(twoListJsonPath);

        return allJsonPath;
    }


    /**
     * 通过jsonPath去获取两个json的value，并校验类型和值是否相等
     * @param jsonPathList
     * @param oneJson
     * @param twoJson
     */
    public static void checkTwoJsonSame(Set jsonPathList,JSONObject oneJson,JSONObject twoJson){
        for (Object jsonPath:jsonPathList){

            Object oneJsonValue = JSONPath.eval(oneJson,jsonPath.toString());
            Object twoJsonValue = JSONPath.eval(twoJson,jsonPath.toString());

            if (!JSONPath.contains(oneJson,jsonPath.toString())){
                System.out.println(String.format("oneJsonPath字段不存在，jsonPath = %s ,value2 = %s",jsonPath,twoJsonValue));
                continue;
            }else if (!JSONPath.contains(twoJson,jsonPath.toString())){
                System.out.println(String.format("twoJsonPath字段不存在，jsonPath = %s ,value1 = %s",jsonPath,oneJsonValue));
                continue;
            }

            if (oneJsonValue.equals(twoJsonValue)){
                System.out.println(String.format("相同，jsonPath = %s，value = %s",jsonPath,oneJsonValue));
            } else if (String.valueOf(oneJsonValue).equals(String.valueOf(twoJsonValue))){
                System.out.println(String.format("字段类型不相同，jsonPath = %s，value1 = %s,value2 = %s",jsonPath,oneJsonValue,twoJsonValue));
            } else {
                System.out.println(String.format("字段值不相同，jsonPath = %s，value1 = %s,value2 = %s",jsonPath,oneJsonValue,twoJsonValue));
            }
        }
    }


    //@Test(description = "判断两个json是否相等")
    public void checkTwoJson(){
        JSONObject oneJsonObject = new JSONObject();
        JSONPath.set(oneJsonObject,"data.person","个人");
        JSONPath.set(oneJsonObject,"data.student.age","20");
        JSONPath.set(oneJsonObject,"data.student.name","张三");

        JSONObject twoJsonObject = new JSONObject();
        JSONPath.set(twoJsonObject,"data.person","个人");
        JSONPath.set(twoJsonObject,"data.student.age",20);

        Set allJsonPath = getTwoJsonAllJsonPath(oneJsonObject,twoJsonObject);
        checkTwoJsonSame(allJsonPath,oneJsonObject,twoJsonObject);
    }

    //@Test(description = "判断两个json共同存在字段的字段值是否相等")
    public void checkTwoJsonSame(){
        JSONObject oneJsonObject = new JSONObject();
        JSONPath.set(oneJsonObject,"data.person","个人");
        JSONPath.set(oneJsonObject,"data.student.age","20");
        JSONPath.set(oneJsonObject,"data.student.name","张三");

        JSONObject twoJsonObject = new JSONObject();
        JSONPath.set(twoJsonObject,"data.person","个人");
        JSONPath.set(twoJsonObject,"data.student.age",20);

        Set sameJsonPath = getTwoJsonSameJsonPath(oneJsonObject,twoJsonObject);
        checkTwoJsonSame(sameJsonPath,oneJsonObject,twoJsonObject);
    }

}
