package com.lst.xmind.entity;

import lombok.Data;
import java.util.List;

@Data
public class Children {
    // 子主题列表
    private List<TopicText> attached;
}
