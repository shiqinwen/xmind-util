package com.lst.xmind.entity;

import lombok.Data;

@Data
public class MarkerRef {
    //图标ID
    private String markerId;
}
