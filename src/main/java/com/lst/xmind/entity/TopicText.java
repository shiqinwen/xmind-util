package com.lst.xmind.entity;

import lombok.Data;
import java.util.List;

/**
 * 主题内容
 */
@Data
public class TopicText {
    // id
    private String id;
    // 主题内容
    private String title;
    // 笔记 如备注
    private Notes notes;
    // 批注
    private List<Comments> comments;
    // 子主题内容
    private Children children;
    // 标签
    private List<Label> labels;
    // 图标
    private MarkerRefs markerRefs;
    // 超链接(网络、文件、主题)
    private String xlinkHref;
}
