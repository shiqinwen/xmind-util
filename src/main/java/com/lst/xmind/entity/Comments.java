package com.lst.xmind.entity;

import lombok.Data;
@Data
public class Comments {
    // 批注时间
    private long creationTime;
    // 批注作者
    private String author;
    // 批注内容
    private String content;
}
