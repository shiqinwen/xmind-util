
package com.lst.xmind.entity;
import lombok.Data;

@Data
public class XmindRoot {
    private String id;
    private String title;
    private TopicText rootTopic;
}
