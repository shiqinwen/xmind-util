package com.lst.xmind.entity;

import lombok.Data;

import java.util.List;

@Data
public class MarkerRefs {
    // 图标列表
    private List<MarkerRef> markerRef;
}
