package com.lst.xmind;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.metadata.CellData;
import com.alibaba.excel.metadata.Head;
import com.alibaba.excel.write.handler.CellWriteHandler;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.alibaba.excel.write.metadata.fill.FillConfig;
import com.alibaba.excel.write.metadata.holder.WriteSheetHolder;
import com.alibaba.excel.write.metadata.holder.WriteTableHolder;
import com.sun.org.slf4j.internal.Logger;
import com.sun.org.slf4j.internal.LoggerFactory;
import lombok.Data;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.File;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * TODO 通过模板导出带公式的数据
 * https://deepmind.t-salon.cc/article/5496
 * https://www.yuque.com/easyexcel/doc/easyexcel 官网文档-旧的
 * https://easyexcel.opensource.alibaba.com/  官网文档-新的
 */
public class ExcelOut {

    public static void main(String[] args) {
        // 1. 路径定义
        // 输入模板路径
        String templatePath = "D:/1.xlsx";
        // 结果输出路径
        String outputPath = "D:/output.xlsx";

        // 2. 真实的模拟数据
        List<DemoData> demoDataList = dataList();

        // 3. 数据写出
        ExcelWriter writer = EasyExcel.write(new File(outputPath)).withTemplate(new File(templatePath)).build();
        // 3.1 操作第一个sheet(记得注册自定义的CellWriteHandler)
        WriteSheet sheet = EasyExcel.writerSheet(0,"导出").registerWriteHandler(new CustomCellWriteHandler()).build();
        // 3.2 填充列表数据
        writer.fill(demoDataList, FillConfig.builder().forceNewRow(Boolean.TRUE).build(), sheet);
        // 3.3 填充其它动态信息
        Map<String, Object> extra = new LinkedHashMap<>();
        // 单位：10%
        extra.put("taxRate", 10);

        //3.4 设置强制计算公式：不然公式会以字符串的形式显示在excel中
        Workbook workbook = writer.writeContext().writeWorkbookHolder().getWorkbook();
        workbook.setForceFormulaRecalculation(true);

        // 3.5 数据刷新
        //writer.fill(extra, sheet);
        writer.finish();

        System.out.println("导入数据成功");
    }

    /**
     * 填充公式
     */
    static class CustomCellWriteHandler implements CellWriteHandler {
        private static final Logger LOGGER = LoggerFactory.getLogger(CustomCellWriteHandler.class);

        @Override
        public void beforeCellCreate(WriteSheetHolder writeSheetHolder, WriteTableHolder writeTableHolder, Row row,
                                     Head head, Integer columnIndex, Integer relativeRowIndex, Boolean isHead) {

        }

        @Override
        public void afterCellCreate(WriteSheetHolder writeSheetHolder, WriteTableHolder writeTableHolder, Cell cell,
                                    Head head, Integer relativeRowIndex, Boolean isHead) {

        }

        @Override
        public void afterCellDataConverted(WriteSheetHolder writeSheetHolder, WriteTableHolder writeTableHolder, CellData cellData, Cell cell, Head head, Integer integer, Boolean aBoolean) {

        }

        /**
         * 处理公式 填充数据
         * @param writeSheetHolder
         * @param writeTableHolder
         * @param cellDataList
         * @param cell
         * @param head
         * @param relativeRowIndex
         * @param isHead
         */
        @Override
        public void afterCellDispose(WriteSheetHolder writeSheetHolder, WriteTableHolder writeTableHolder,
                                     List<CellData> cellDataList, Cell cell, Head head, Integer relativeRowIndex, Boolean isHead) {
            // 这里可以对cell进行任何操作
            System.out.println("进入第" +  cell.getRowIndex() + "行,第" + cell.getColumnIndex() + "列数据...");
            if (cell.getRowIndex() >= 1 && 3 == cell.getColumnIndex()) {
                // 交易得分 新增、维护接口/交易数得分=数量*1.5，每个月完成10个及以上得15分
                // 公式 IF(C3*1.5>=15,15,C3*1.5)
                int actualCellRowNum = cell.getRowIndex() + 1;
                String formula = "IF(C" + actualCellRowNum +"*1.5>=15,15,C" + actualCellRowNum + "*1.5)";
                System.out.println("公式:" + formula);
                cell.setCellFormula(formula);
                System.out.println(cell.getCellFormula());
                System.out.println("第" +  cell.getRowIndex() + "行,第" + cell.getColumnIndex() + " 交易得分完成");
            }

            /*if (cell.getRowIndex() >= 1 && 5 == cell.getColumnIndex()) {
                // 总价 = 含税单价 * 数量
                // 以第4行数据为例：税价 = C5*D5
                int actualCellRowNum = cell.getRowIndex() + 1;
                cell.setCellFormula("C" + actualCellRowNum + "*D" + actualCellRowNum);
                System.out.println("第" +  cell.getRowIndex() + "行,第" + cell.getColumnIndex() + "总价写入公式完成");
            }
            if (cell.getRowIndex() >= 2 && 8 == cell.getColumnIndex()) {
                // 总价 = 含税单价 * 数量
                // 以第4行数据为例：税价 = C5*D5
                int actualCellRowNum = cell.getRowIndex() + 1;
                cell.setCellFormula("C" + actualCellRowNum + "*D" + actualCellRowNum);
                System.out.println("第" +  cell.getRowIndex() + "行,第" + cell.getColumnIndex() + "总价写入公式完成");
            }
            if (cell.getRowIndex() >= 2 && 10 == cell.getColumnIndex()) {
                // 总价 = 含税单价 * 数量
                // 以第4行数据为例：税价 = C5*D5
                int actualCellRowNum = cell.getRowIndex() + 1;
                cell.setCellFormula("C" + actualCellRowNum + "*D" + actualCellRowNum);
                System.out.println("第" +  cell.getRowIndex() + "行,第" + cell.getColumnIndex() + "总价写入公式完成");
            }*/
        }
    }

    /**
     * 模拟的数据列表
     * @return 列表
     */
    private static List<DemoData> dataList() {
        ArrayList<DemoData> list = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            DemoData demoData = new DemoData();
            demoData.setName("张三" + i);
            demoData.setUserType("自有人员");
            demoData.setTradeNum("1"+ i);
            demoData.setTradeScope("");
            demoData.setFlowNum("2" + i);
            demoData.setFlowScode("");
            demoData.setCaseAddNum("3" + i);
            demoData.setCaseAddScope("");
            demoData.setCaseUpdateNum("4"+i);
            demoData.setCaseUpdateScope("");
            demoData.setWorkScope("");
            list.add(demoData);
        }
        return list;
    }


    @Data
    static class DemoData implements Serializable {
        private String name;
        private String userType;
        private String tradeNum;
        private String tradeScope;
        private String flowNum;
        private String flowScode;
        private String caseAddNum;
        private String caseAddScope;
        private String caseUpdateNum;
        private String caseUpdateScope;
        private String workScope;

    }
}
